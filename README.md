# Bước Đầu Học Phật - Thiền Sư Thích Thanh Từ #

Đây là kho mã nguồn lưu trữ toàn bộ tiến trình thực hiện việc chép lại quyển sách tiêu đề như trên, với công cụ là \LaTeX.

Mọi theo dõi, đóng góp đều hoan nghênh.

## Người mở kho ##

* xuansamdinh

## Về Kho:

- Mục đích sao chép, tái bản quyển sách như tiêu đề trên của tác giả.
- Người chép chỉ thực hiện việc sao chép, bố cục chương mục theo dạng sách, không thay đổi, tác động nội dung.
- Mọi quyền hạn là của tác giả.

## Downloads

Tải xuống ebook này tại mục [Downloads](https://bitbucket.org/xuansamdinh/buoc-dau-hoc-phat/downloads)

Bao gồm 2 bản:

- **ebook.pdf**: đây là bản xem trên các thiết bị điện tử, các đường link, liên kết, có màu nhằm tiên lợi truy xuất, theo dõi. Bản này nếu in thành sách, các dòng chữ, đường link có màu sẽ nhạt hơn với dạng in đen - trắng.
- **print.pdf**: là bản dành cho xuất bản desktop, dùng để in thành sách, chữ chỉ một màu đen.